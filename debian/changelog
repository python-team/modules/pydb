pydb (1.26-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * d/watch: Use https protocol
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:02:13 -0500

pydb (1.26-2) unstable; urgency=low

  * Team Upload

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Mattia Rizzolo ]
  * Move to source format 3.0 (quilt), drop dpatch build-dependency.
  * Migrate from pysupport to dh_python2.  (Closes: #786184)

 -- Mattia Rizzolo <mattia@debian.org>  Fri, 11 Dec 2015 22:09:20 +0000

pydb (1.26-1) unstable; urgency=low

  * Acknowledge the NMU by Simon McVittie.
  * New upstream release.
  * debian/control: Update standards-version.

 -- Oleksandr Moskalenko <malex@debian.org>  Sun, 01 Nov 2009 22:49:37 -0600

pydb (1.25-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Assume the directory layout seen in python-support 1.0 rather than
    0.8.x, with modules in /usr/share/pyshared (Closes: #516324)
  * Do the initial installation into the directories used by the default
    version of Python, not 2.4
  * Don't sed away a wrong path in the man page that actually no longer exists
  * Lintian fixes:
    - Move python-support to Build-Depends-Indep and do the build in
      build-indep, not build-arch, as this package is architecture-independent
    - Move menu entry from obsolete Apps to Applications
    - Install upstream ChangeLog as conventional changelog.gz, not ChangeLog.gz
    - Copy the actual copyright statements to debian/copyright, not just the
      upstream author's name
    - debian/patches/01_man_hyphens.dpatch: replace all "-" with either "\-"
      (minus) or "\(hy" (hyphen) depending on apparent intention, according to
      the best practice explained by lintian
    - debian/patches/02_man_Sp.dpatch: Remove undefined Sp macro (the man
      page seems to look fine without it)
    - debian/README.Debian: fix a spelling mistake

 -- Simon McVittie <smcv@debian.org>  Tue, 04 Aug 2009 22:48:49 +0100

pydb (1.25-1) unstable; urgency=low

  [Sandro Tosi]
  * debian/control
    - switch Vcs-Browser field to viewsvn
  [Oleksandr Moskalenko]
  * New upstream release.
  * debian/pydb.docs: Added upstream ChangeLog to docs (Closes: #508414).
  * debian/{pydb.emacsen-install,pydb.emacsen-remove,pydb.emacsen-startup}:
    Added emacse setup files again (Closes: #493190).
  * debian/control:
    - Changed Depends on emacsen-common to Suggests emacs22 (the only version
      pydb.el works with).
    - Updated standards-version and debhelper dependencies.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 26 Jan 2009 13:44:36 -0700

pydb (1.23-2) unstable; urgency=low

  [Carlos Galisteo]
  * debian/control
    - Added Homepage field.
  [Oleksandr Moskalenko]
  * debian/control: Added a dependency on dpatch.
  * debian/rules:
    - Added .PHONY patch and unpatch targets
    - Included /usr/share/dpatch/dpatch.make and added patch and unpatch
      targets.
  * debian/patches/01_pydbbdp.py.dpatch: ipython fix patch from the upstream
    author.
  * Removed all emacsen install remove and startup files. Just install pydb.el
    and run dh_installemacsen (Closes: #443351).

 -- Oleksandr Moskalenko <malex@debian.org>  Sun, 20 Jul 2008 21:55:31 -0600

pydb (1.23-1) unstable; urgency=low

  * New upstream release.

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 02 Jul 2008 13:31:28 -0600

pydb (1.22-3) unstable; urgency=low

  * debian/control: fix the short description field (Closes #441230).

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 07 Sep 2007 13:32:36 -0600

pydb (1.22-2) unstable; urgency=low

  * Fix emacsen-install (Closes: #421298). Thanks to Luis Rodrigo Gallardo
    Cruz <rodrigo@nul-unu.com> for the suggestion.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 16 Apr 2007 17:04:06 -0600

pydb (1.22-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Added a watch file.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 16 Apr 2007 13:07:36 -0600

pydb (1.20-2) unstable; urgency=low

  * debian/control:
    - Joined the Debian Python Modules Team, so added
      <python-modules-team@lists.alioth.debian.org> to Uploaders.
    - Added XS-Vcs-Svn and XS-Vcs-Browser control fields.

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 31 Jan 2007 13:10:57 -0700

pydb (1.20-1) unstable; urgency=low

  * New bugfix upstream release on Dec 10th.
  * debian/control: Added emacsen-common to the depends to be able to install
    emacs mode.
  * debian/pydb.dirs: Added a line for usr/share/emacs/site-lisp/pydb.
  * Added debian/pydb.install file.
  * debian/pydb.install: Added a line to install the pydb.el file.
  * Added and modified debian/ emacsen-install, emacsen-remove,
    emacsen-startup files (Closes: #399944).

 -- Oleksandr Moskalenko <malex@debian.org>  Sun, 10 Dec 2006 19:44:03 -0700

pydb (1.19-1) unstable; urgency=low

  * New upstream release on Oct 26th.
  * debian/control: Removed python from "Depends" as ${python:Depends} takes
    care of that.

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 27 Oct 2006 11:31:21 -0600

pydb (1.18-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: Updated the sed rule that fixes the path to the pydb.doc
    in the man page as the upstream fixed pdb.doc --> pydb.doc.

 -- Oleksandr Moskalenko <malex@debian.org>  Tue,  3 Oct 2006 18:11:48 -0600

pydb (1.17-1) unstable; urgency=low

  * New upstream release on July 29th, 2006. Pydb is now developed by Rocky
    Bernstein <rocky@panix.com>. See http://bashdb.sourceforge.net/pydb/ for
    more information on how it differs from the old (1.01) pydb.
  * debian/README.Debian: Added a note from the new upstream author explaining
    the pydb evolution to the present day.
  * debian/copyright: Updated the copyright file to reflect the switch to new
    upstream and new license GPLv2.
  * debian/control:
    + Updated Standards-Version to 3.7.2.
    + Added a dependency on python-support (>= 0.4) per new
      python policy.
    + Removed version from the ddd suggests (>= 3.0-5).
    + Changed build-depends-indep to build-depends per linda warning.
  * debian/rules:
    + Call dh_pysupport instead of dh_python per new python policy.
    + Added a "DEBIAN_DIR" variable.
    + Added a dh_clean rule to the build-stamp target.
    + Added the build commands - ./configure, make, make install to teh
      build-stamp target.
    + Added DEB_HOST_GNU_TYPE and DEB_BUILD_GNU_TYPE variable expansion.
    + Added rules for setting CFLAGS
    + Remove configure generated files in the clean target.
  * debian/pydb.1: Switched to the upstream manpage.

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 16 Aug 2006 19:26:50 -0600

pydb (1.01-10) unstable; urgency=low

  * debian/compat: Changed compatibility level to 5, the latest.
  * debian/control: Updated debhelper dependency to reflect the new
    compatibility level.
  * debian/control: Changed the maintainer email to ...debian.org.

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 10 Jan 2006 09:08:02 -0700

pydb (1.01-9) unstable; urgency=low

  * New maintainer (Closes: #312850). Thanks to Gregor Hofleit and NMU
    uploaders for their work.
  * Acknowledge NMUs (Closes: #190662, #213878, #211787).
  * debian/control: Updated Standards-Version to 3.6.2.
  * debian/control: Changed Maintainer to my own name.
  * debian/compat: Added this file to set the DH_COMPAT level to 4.
  * debian/rules: Removed unnecessary comments.
  * debian/rules: Fixed build-time installation paths from debian/tmp to
    debian/pydb.
  * debian/pydb.1: Updated the location of pdb.doc.
  * debian/rules: Use dh_installman instead of the deprecated
    dh_installmanpages for man page installation.
  * pydb.py: Updated the file directly instead of patching it as there is no
    upstream anymore (Closes: #213214).

 -- Oleksandr Moskalenko <malex@tagancha.org>  Tue, 23 Aug 2005 23:00:43 -0600

pydb (1.01-8.3) unstable; urgency=low

  * NMU (using the prepared package from Oleksandr Moskalenko).
  * Fix breakpoint set interaction with ddd (Closes: #190662).
  * Quote all debian/menu entries (lintian warning)
  * Use newer standards version (lintian warning)

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  9 Mar 2005 17:56:58 +0100

pydb (1.01-8.2) unstable; urgency=high

  * NMU.
  * Compensate for missing debhelper dependencies, depend on python.
    Closes: #213878.

 -- Matthias Klose <doko@debian.org>  Fri, 10 Oct 2003 08:10:31 +0200

pydb (1.01-8.1) unstable; urgency=low

  * NMU.
  * Fix postinst/prerm (closes: #211787).

 -- Matthias Klose <doko@debian.org>  Sun, 28 Sep 2003 13:39:02 +0200

pydb (1.01-8) unstable; urgency=low

  * Make the package now use Python 2.3, which is the new default (closes:
    #205309).

 -- Gregor Hoffleit <flight@debian.org>  Tue,  2 Sep 2003 19:27:44 +0200

pydb (1.01-7) unstable; urgency=low

  * Make the package use Python 2.2 (closes: #140810, #161553).

 -- Gregor Hoffleit <flight@debian.org>  Tue, 24 Sep 2002 14:39:02 +0200

pydb (1.01-6) unstable; urgency=low

  * Fix manpage (point to /usr/lib/python2.1/pdb.doc).

 -- Gregor Hoffleit <flight@debian.org>  Sun, 14 Apr 2002 13:44:55 +0200

pydb (1.01-5) unstable; urgency=low

  * Remove python1.5ism in postinst script (closes: #133520).

 -- Gregor Hoffleit <flight@debian.org>  Tue, 12 Feb 2002 19:21:49 +0100

pydb (1.01-4) unstable; urgency=high

  * Remove use of newdir, which is obsolete since Python 1.5 (apply a patch
    from ddd/pydb) (closes: #127408).
  * Make the package use Python 2.1.

 -- Gregor Hoffleit <flight@debian.org>  Thu, 24 Jan 2002 15:18:00 +0100

pydb (1.01-3) unstable; urgency=low

  * Repackaged to depend on python1.5 (closes: #119203).

 -- Gregor Hoffleit <flight@debian.org>  Thu, 13 Dec 2001 15:42:54 +0100

pydb (1.01-2) unstable; urgency=low

   * Updated package for policy 3.1.1:
     - FHS transition of docs
     - Build-Depends
     (closes: #80319).
   * Added hint to the menu file (closes: #80244).

 -- Gregor Hoffleit <flight@debian.org>  Mon, 25 Dec 2000 22:41:32 +0100

pydb (1.01-1) unstable; urgency=low

  * Initial Release.
  * pydbcmd.py: Included functions from newdir.py, which is no
    longer included in Python.

 -- Gregor Hoffleit <flight@debian.org>  Sun, 22 Aug 1999 23:02:50 +0200
